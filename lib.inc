section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .cicle:
    	cmp byte [rdi+rax], 0;
    	jnz .inc_count
    	ret
    .inc_count:
    	inc rax
    	jmp .cicle	

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1 
    syscall 
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10
    push 0
    .cicle:
    	xor rdx, rdx
    	div r10
    	add rdx, '0'
    	push rdx
    	cmp rax, 0
    	jnz .cicle
    .print:
    	pop rdi
    	cmp rdi, 0
    	je .exit
    	call print_char
    	jmp .print
    .exit:
    	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .minus
    jmp print_uint
    .minus:
    	push rdi
    	mov rdi, '-'
    	call print_char
    	pop rdi
    	neg rdi
    	call print_uint
    	ret
    	

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    mov rdx, rax
    call string_length
    cmp rax, rdx
    jne .falce
    xor rax, rax
    push rbx
    .cicle:
    	mov bl, byte [rsi+rax]
    	cmp byte [rdi+rax], bl
    	jne .falce
    	cmp byte [rdi+rax], 0
    	je .exit
    	inc rax
    	jmp .cicle
    .falce:
    	xor rax, rax
    	pop rbx
    	ret
    .exit:
    	mov rax, 1
    	pop rbx
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    mov r10, rsi
    mov r9, rdi
    .check:
	call read_char
	test al, al
	jz .error
	cmp al, " "
	je .check
	cmp al, "\n"
	je .check
	cmp al, 0x9
	je .check
    .cicle:
        dec r10
	jz .error
	mov byte[r9], al
	inc r9
	call read_char
	test al, al
	jz .exit
	cmp al, " "
	je .exit
	cmp al, "\n"
	je .exit
	cmp al, 0x9
	je .exit
	jmp .cicle
     .error:
        add rsp, 8
	xor rax, rax
	xor rdx, rdx
        ret
    .exit:
        mov byte[r9], 0
	pop rax
	mov rdx, r9
	sub rdx, rax
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r10, 10
    xor rsi, rsi
    xor rdx, rdx
.cicle:
    movzx r8, byte[rdi + rcx]
    cmp r8b, '9'
    ja .exit
    cmp r8b, '0'
    jb .exit
    xor rdx, rdx
    mul r10
    sub r8b, '0'
    add rax, r8
    inc rcx
    jmp .cicle
.exit:
        mov rdx, rcx
    	ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne .plus
    inc rdi
    call parse_uint  
    cmp rdx, 0
    je .exit
    neg rax
    inc rdx 
    ret
    .plus:
    	call parse_uint
    	ret
    .exit:
    	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r10, r10
    xor rax, rax
    push rdi
    mov rdi, rsi
    pop rsi
    call string_length
    mov rcx, rax
    rep movsb
    mov rax, r10
    xor r10, r10
    ret
